package br.edu.cest.prova;

import java.math.BigDecimal;

public class Item {
	private String title;
	private String publisher;
	private String yaerpublished;
	private String isbn;
	private BigDecimal price;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getYaerpublished() {
		return yaerpublished;
	}
	public void setYaerpublished(String yaerpublished) {
		this.yaerpublished = yaerpublished;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
}
